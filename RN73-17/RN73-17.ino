#include <Arduino_FreeRTOS.h>

void setup() {
  start_serial();
  initialize();
}

void start_serial() {
  Serial.begin(9600);
  while(!Serial);
  Serial.println(F("Serial is ready"));
  Serial.println();
}

// =====================================================
// *******************INITIALIZATION********************
// =====================================================

void initialize() {
  initialize_pins();
  initialize_semaphores();
  initialize_tasks();
}

#define LED_1 5

#define LED_2 6

#define LED_3 7

#define NUMBER_OF_LEDS 3

const int leds[NUMBER_OF_LEDS] = { LED_1, LED_2, LED_3 };

void initialize_pins() {
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);
}

SemaphoreHandle_t semaphore1;

SemaphoreHandle_t semaphore2;

SemaphoreHandle_t semaphore3;

void initialize_semaphores() {
  semaphore1 = xSCreate();
  semaphore2 = xSCreate();
  semaphore3 = xSCreate();
}

TaskHandle_t task1_handle;

TaskHandle_t task2_handle;

TaskHandle_t task3_handle;

void initialize_tasks() {
  initialize_task1();
  initialize_task2();
  initialize_task3();
}

void initialize_task1() {
  xTaskCreate(task1, (const portCHAR *)"task1", 256, NULL, 3, &task1_handle);
  xAttach(semaphore1, task1_handle);
  xAttach(semaphore2, task1_handle);
}

void initialize_task2() {
  xTaskCreate(task2, (const portCHAR *)"task2", 256, NULL, 2, &task2_handle);
  xAttach(semaphore3, task2_handle);  
}

void initialize_task3() {
  xTaskCreate(task3, (const portCHAR *)"task3", 256, NULL, 1, &task3_handle);
  xAttach(semaphore2, task3_handle);
  xAttach(semaphore3, task3_handle);  
}

// =====================================================
// ************************LEDS*************************
// =====================================================

void turn_leds_off() {
  for (int i = 0; i < NUMBER_OF_LEDS; i++) 
    digitalWrite(leds[i], LOW);
}

void turn_led_off(int led) {
  digitalWrite(led, LOW);
}

void turn_led_on(int led, int duration) {
  for (int i = 0; i < duration; i++) {
    digitalWrite(led, HIGH);
    delay(200);
  }
}

void blink_led(int led, int duration) {
  for (int i = 0; i < duration; i++) {
    digitalWrite(led, HIGH);
    delay(200);
    digitalWrite(led, LOW);
    delay(200);
  }
}

// =====================================================
// ************************TASKS************************
// =====================================================

void task1(void *pvParameters) {
  vTaskDelay(5000 / portTICK_PERIOD_MS);
  
  turn_led_on(LED_1, 5);

  wait_semaphore(semaphore1, LED_1, 5);
  
  signal_semaphore(semaphore1, LED_1, 5);

  wait_semaphore(semaphore2, LED_1, 5);
  
  signal_semaphore(semaphore2, LED_1, 5);
  
  turn_led_off(LED_1);
  vTaskSuspend(NULL);
  while(1);
}

void task2(void *pvParameters) {
  vTaskDelay(2000 / portTICK_PERIOD_MS);
  
  turn_led_on(LED_2, 5);

  wait_semaphore(semaphore3, LED_2, 5);

  signal_semaphore(semaphore3, LED_2, 5);

  turn_led_off(LED_2);
  vTaskSuspend(NULL);
}

void task3(void *pvParameters) {
  turn_led_on(LED_3, 5);
  
  wait_semaphore(semaphore3, LED_3, 5);

  wait_semaphore(semaphore2, LED_3, 5);

  signal_semaphore(semaphore2, LED_3, 5);

  signal_semaphore(semaphore3, LED_3, 5);

  turn_led_off(LED_3);
  vTaskSuspend(NULL);
}

void wait_semaphore(SemaphoreHandle_t semaphore, int led, int duration) {
  xSWait(semaphore);
  blink_led(led, duration);
}

void signal_semaphore(SemaphoreHandle_t semaphore, int led, int duration) {
  xSSignal(semaphore);
  turn_led_on(led, duration);
}

void loop() {}
